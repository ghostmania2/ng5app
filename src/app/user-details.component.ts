import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector:'user-details',
  templateUrl: './user-details.component.html'

})
export class UserDetailsComponent{
  @Input() name: string;
  @Input() age: string;
  @Output() invite = new EventEmitter<{}>();
  @Output() nameChange = new EventEmitter<string>();
  @Output() ageChange = new EventEmitter<string>();
  handleClick(value){
    if (value.indexOf('giphy2') > -1) {
      this.userImage = '../assets/images/giphy.gif';
    } else {
      this.userImage = '../assets/images/giphy2.gif';
    }
  }
  handleNameChange(e){
    this.nameChange.emit(e.target.value)
  }
  handleAgeChange(e){
    this.ageChange.emit(e.target.value)
  }
  userImage = '../assets/images/giphy.gif';


}

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ng5Application';
  user1 = {name: 'petr', age: 23};
  user2 = {name: 'Vasya', age: 42};

  doInvite(name: string){
    console.log('user invited ', name);
  }
}
